package jp.alhinc.usui_arisa.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		//コマンドライン引数のエラー表示
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}

		Map<String, String>branchName = new HashMap<String, String>();

		Map<String, Long> sales = new HashMap<String, Long>();

		// System.out.println("ここにあるファイルを開きます >=" + args[0]);
		if(!inputFile(args[0], "branch.lst", branchName, sales)) {
			return ;
		}

		File[] files = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			// System.out.println(files[i].getName());

			String FileName = files[i].getName();
			if (FileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}

		}

		//売上ファイルが連番になっていないか場合、エラー表示する
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			// 比較元( i )
			// ファイル一つ取ってくる
			File target = rcdFiles.get(i);
			// ファイルの名前取ってくる
			String fileName = target.getName();
			// System.out.println(fileName+ "を表示する");
			// 「.rcd」を削除
			String numberForStr = fileName.substring(0, 8);
			//文字列の数字から数字型に変換
			int numberForInt = Integer.parseInt(numberForStr);

			// 比較先( i + 1 )
			File another = rcdFiles.get(i + 1);
			//ファイルの名前とってくる
			String Name = another.getName();
			// System.out.println(Name + "比較もとに＋１したものを表示");
			//文字列の一部を取得
			String number = Name.substring(0, 8);
			//文字列の数字から数字型に変換
			int change = Integer.parseInt(number);

			//条件（連番）になっていないとき。差分が１
			if (change - numberForInt != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}


		//売上ファイルを読み込んで、中身を確認。コンソールに出力する
		//ファイル情報を入れる箱（リスト）を作成

		for (int i = 0; i < rcdFiles.size(); i++) {
			List<String> fileContents = new ArrayList<String>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(rcdFiles.get(i)));

			String row = "";
			while ((row = br.readLine()) != null) {
					fileContents.add(row);

			}
			//売上ファイルの中身確認、2行なのか
			if(fileContents.size() != 2) {
				System.out.println(rcdFiles.get(i).getName()+"のフォーマットが不正です。");
				return;

			}
			//売上ファイルの売上金額が数字ではなかった場合
			if(!fileContents.get(1).matches("^[0-9]+$")) {
				System.out.println("予期せぬエラーが発生いたしました。");
				return;
			}

			//Longに置き換え、売上金額を取り出す
			long line = Long.parseLong(fileContents.get(1));
			//売上ファイルの支店コードが支店定義ファイルに該当しなかった場合
			if (!branchName.containsKey(fileContents.get(0))){
			// System.out.println("存在します");
				 System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です。");
				return;
			}
			//Mapから取り出す支店№
			String branchCord = (fileContents.get(0));
			long sale = (sales.get(branchCord));




			//売上金額と支店№（変数に入れる）
			long sum = line + sale;
			//集計した売上金額が10桁を超えた場合
			if (sum >= 1000000000L) {
				System.out.println("合計金額が10桁を超えました。");
				return;
			}

			sales.put(branchCord, sum);

			} catch (IOException error) {
				System.out.println("予期せぬエラーが発生しました。");
				return;

			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}

		if(!outputFile(args[0] , "branch.out", branchName ,sales)) {
			return;
		}
	}
	public static boolean inputFile(String path, String files, Map<String, String>Names, Map<String, Long> sales) {

		BufferedReader br = null;
		try {

			File file = new File(path, files );

			//エラー処理
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません。");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				// System.out.println(line);

				String[] items = line.split(",");

				Names.put(items[0], items[1]);
				sales.put(items[0], 0L);

				if (items.length != 2 || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return false;
				}

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}



	public static boolean outputFile(String path ,String fileName, Map<String, String>names , Map<String, Long> sales) {
		BufferedWriter bw = null;
		//ファイルを作成
		try {
			File file = new File(path, fileName);
			//ファイルに書き込み
			FileWriter branch = new FileWriter(file);

			bw = new BufferedWriter(branch);

			//支店No.支店名、売上金額を改行させて書き込む
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}



